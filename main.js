var express = require("express");
var path = require("path");
var signupAppEngine = require("./signupAppEngine.js");
var bodyParser = require('body-parser');

var mySignupEngine = signupAppEngine();

var app = express();


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


app.use(express.static(path.join(__dirname, "public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components")));

app.get("/countryList", function(req, resp) {
    resp.status(200)
    resp.type("application/json");
    resp.json(mySignupEngine.countryList);
});

app.get("/memberList", function(req, resp) {
    resp.status(200)
    resp.type("application/json");
    resp.json(mySignupEngine.signUpList);
});

app.post("/handleSignUp", function(req, resp) {
    console.log(req.body.data);
    mySignupEngine.addSignUp(req.body.data);

    resp.status(200).end();
});



// Catch all
app.use(function (req, resp) {
    resp.status(440);
    resp.send("Error File not Found");
});

// set port and start webserver
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);
app.listen(app.get("port"), function () {
    console.info("Application started at %s is listening to port %d", new Date(), app.get("port"));
});