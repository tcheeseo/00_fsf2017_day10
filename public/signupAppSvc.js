(function() {

    var SignUpAppSVC = angular.module("SignUpAppSVC", []);

    var SignUpSVC = function($http) {
        var signUpSVC = this;

        signUpSVC.handleSignup = function(signupData) {
            return ($http.post("/handleSignUp", {
                    data: signupData
            }));
        };

        signUpSVC.getCountryList = function() {
            return ($http.get("/countryList").then(function (response) {
                return response.data;
            }));
        }

        signUpSVC.getMemberList = function() {
            return ($http.get("/memberList").then(function (response) {
                return response.data;
            }));
        }


    };


    SignUpAppSVC.service("SignUpSVC", ["$http", SignUpSVC]);
})();
