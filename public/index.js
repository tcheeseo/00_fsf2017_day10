(function () {

    var SignupApp = angular.module("SignupApp", ["SignUpAppSVC"]);

    var SignupCtrl = function (SignUpSVC) {
        var signupCtrl = this;

        var initSignupField = function() {
            signupCtrl.username = "";
            signupCtrl.useremail = "";
            signupCtrl.userpassword = "";
            signupCtrl.gender = "Male"; 
            signupCtrl.dateOfBirth = new Date();
            signupCtrl.address = "";
            signupCtrl.contact = "";
            signupCtrl.countrySelected = "";
            signupCtrl.showError = false;
        }
        initSignupField();

        signupCtrl.countryList = [];       
        signupCtrl.signUpMsg = "";

        signupCtrl.getCountryList = function() {
            SignUpSVC.getCountryList().then(function (resultList) {
                signupCtrl.countryList = resultList;
            });
        }
        signupCtrl.getCountryList();


        signupCtrl.isValidPassword = function() {
            var regularExpression = /^(?=.*\d)(?=.*[@#$])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            return regularExpression.test(signupCtrl.userpassword);
        }

        signupCtrl.isValidContact = function() {
            var regularExpression = /^[0-9 ()+-]+$/;
            return regularExpression.test(signupCtrl.contact);
        }

        signupCtrl.isValidAge = function () {
            var age = new Date().getYear() - signupCtrl.dateOfBirth.getYear();
            //console.log("Age : %d", age);
            return (age >= 18);
        }

        signupCtrl.isValidSubmit = function () {
            signupCtrl.showError = true;
            //console.log("Password [%s] vaild %s", signupCtrl.userpassword , signupCtrl.isValidPassword());
            //console.log("Contact [%s] vaild %s", signupCtrl.contact, signupCtrl.isValidContact());
            //console.log("Valid Age: " + signupCtrl.isValidAge());

            return (signupCtrl.isValidPassword() && signupCtrl.isValidContact() && signupCtrl.isValidAge());
        }

        signupCtrl.handleSignUp = function() {
            if (signupCtrl.isValidSubmit()) {
                SignUpSVC.handleSignup({
                        username: signupCtrl.username,
                        useremail: signupCtrl.useremail,
                        userpassword: signupCtrl.userpassword,
                        gender: signupCtrl.gender, 
                        dateOfBirth: signupCtrl.dateOfBirth,
                        address: signupCtrl.address,
                        contact: signupCtrl.contact,
                        country: signupCtrl.countrySelected
                }).then(function (response) {
                    initSignupField();
                    signupCtrl.signUpMsg = "Registration Success! You will be redirect to the Login Page";
                    setTimeout(function(){ window.location.replace("/login.html"); }, 3000);
                });
            }
        }
    };

    SignupApp.controller("SignupCtrl", ["SignUpSVC", SignupCtrl]);
})();