(function () {
    var ViewSignUpApp = angular.module("ViewSignUpApp", ["SignUpAppSVC"]);

    var ViewSignCtrl = function (SignUpSVC) {
        var viewSignCtrl = this;

        viewSignCtrl.memberList = [];

        viewSignCtrl.getMemberList = function() {
            SignUpSVC.getMemberList().then(function (resultList) {
                viewSignCtrl.memberList = resultList;
            });
        }
        viewSignCtrl.getMemberList();
    };
    ViewSignUpApp.controller("ViewSignCtrl", ["SignUpSVC", ViewSignCtrl]);
})();